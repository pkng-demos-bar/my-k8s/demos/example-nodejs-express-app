# Example NodeJS Express App

This project contains the following
- a sample webapp (built using Express v4.18) with the following endpoints available:
    - http://localhost:3000/
    - http://localhost:3000/json
    - http://localhost:3000/text
- a sample test in a /test sub directory
- a .devfile.yaml to run this project in gitlab workspaces

# Running the project in Gitlab Workspaces

- In the workspace, first install the dependencies using `npm install`
- After this, webapp can be started using `npm start`
- The test can be run using `npm test`

# What next

Go ahead, make sure changes and play around with it!

